#include <sys/sysinfo.h>
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char** argv) {
	struct sysinfo info;
	sysinfo(&info);
	printf("Total RAM: %lu\n", info.totalram);	
	exit(0);
}
